<?php

/**
 * @file
 * Hook_requirements for the SAML2 Service Provider module.
 */


/**
 * Implements hook_schema().
 */
function saml2_sp_schema() {
  $schema = array();

  // Cache all outbound SAML2 SP requests.
  $schema['saml2_sp_request_tracking_cache'] = drupal_get_schema_unprocessed('system', 'cache');


  // Store the IDP data.
  $schema['saml2_sp_idps'] = array(
    'description' => 'IdPs registered with SAML2 Service Provider.',

    // Enable CTools exportables based on this table.
    'export' => array(
      // SAML2 IdP machine name key.
      'key' => 'machine_name',
      // In the export, entries will be identified as $idp.
      'export' => 'idp',
      // Description of key.
      'key name' => 'IDP machine name',
      // Variable name to use in exported code.
      'identifier' => 'saml2_idp',

      // Use the same hook as the API name below.
      'default hook' => 'saml2_sp_default_idps',
      // CTools API implementation.
      'api' => array(
        'owner' => 'saml2_sp',
        // Base name for API files: foo.saml2_sp_idps.inc
        'api' => 'saml2_sp_idps',
        'minimum_version' => 1,
        'current_version' => 1,
      ),
    ),

    'fields' => array(
      'machine_name' => array(
        'description' => 'Unique identifier for the environment',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Human-readable name for the SAML2 IdP',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'app_name' => array(
        'description' => 'Human-readable name to provide to the IdP to identify the application',
        'type' => 'varchar',
        'length' => 30,
        'not null' => TRUE,
        'default' => '',
      ),
      'login_url' => array(
        'description' => 'Full url to connect to the SAML2 login endpoint',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'logout_url' => array(
        'description' => 'Full url to connect to the SAML2 logout endpoint',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ),
      'x509_cert' => array(
        'description' => 'The x.509 public certificate of the IdP',
        'type' => 'text',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('machine_name'),
  );

  return $schema;
}
