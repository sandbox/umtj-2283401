<?php

/**
 * @file
 * Admin pages for the SAML2 Drupal Login module
 */

/**
 * Configure which IdP to use when authenticating with Drupal.
 */
function saml2_sp_drupal_login_admin_config_form($form, &$form_state) {
  // List all the IdPs in the system.
  foreach (saml2_sp_load_all_idps() as $machine_name => $idp) {
    $idps[$machine_name] = $idp->name;
  }

  $form['saml2_sp_drupal_login_idp'] = array(
    '#type' => 'select',
    '#options' => $idps,
    '#title' => t('IdP'),
    '#description' => t('Choose the IdP to use when authenticating Drupal logins'),
    '#default_value' => variable_get('saml2_sp_drupal_login_idp', ''),
  );
  return system_settings_form($form);
}
