SAML2 Service Provider
=====================

This package provides two modules:
- SAML2 Service Provider API
- SAML2 Drupal Login


The API module lets other modules leverage SAML2 authentication.

The SAML2 Drupal Login module specifically enables Drupal to become a "Service
Provider" for an IdP, so users can authenticate to Drupal (without entering a
username or password) by delegating authenticate to a SAML2 IdP (Identity
Provider).


Dependencies
============

Requires the OneLogin SAML-PHP toolkit, downloaded to your 'libraries' folder:

`cd libraries`
`git clone https://github.com/onelogin/php-saml.git .`

- libraries
  - php-saml
    - ext
      - xmlseclibs
        - xmlseclibs.php
